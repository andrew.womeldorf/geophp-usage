# GeoPHP Usage

Dockerfile to build an image compatible to run [GeoPHP](https://geophp.net).

I found the GeoPHP site to be relatively unhelpful for setup, as most of the example setups were outdated. This repo provides the ability to create a container to run GeoPHP functions.

## Usage

```
# Build Image
docker build ./ -t geos

# Run Container
docker run -it --rm -v `pwd`:/var/www/html geos php <PHPFILE>
```
