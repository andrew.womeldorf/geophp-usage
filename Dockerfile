FROM php:7.1.20-apache

RUN apt-get -y update --fix-missing && apt-get upgrade -y
RUN apt-get -y install build-essential autotools-dev autoconf automake libtool

# GEOS
RUN curl -fsSL 'http://download.osgeo.org/geos/geos-3.7.2.tar.bz2' -o geos-3.7.2.tar.bz2 \
    && tar -xjvf geos-3.7.2.tar.bz2 \
    && rm geos-3.7.2.tar.bz2

RUN cd geos-3.7.2 \
    && ./configure \
    && make \
    && make install

# PHP-GEOS
RUN curl -fsSL 'https://git.osgeo.org/gitea/geos/php-geos/archive/1.0.0.tar.gz' -o php-geos.tar.gz \
    && tar -xf php-geos.tar.gz \
    && rm php-geos.tar.gz

RUN cd php-geos \
    && ./autogen.sh \
    && ./configure \
    && make \
    && make install


ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

RUN docker-php-ext-enable geos.so
