<?php

include_once dirname(__DIR__, 2) . '/geoPHP/geoPHP.inc';

$tzList = [
    'America__New_York' => 'America/New_York',
    'America__Chicago' => 'America/Chicago',
    'America__Denver' => 'America/Denver',
    'America__Los_Angeles' => 'America/Los_Angeles',
    'America__Anchorage' => 'America/Anchorage',
    'Pacific__Honolulu' => 'Pacific/Honolulu',
];

// First arg is LONGITUDE
// Second arg is LATITUDE
$point = new Point(-88.2019397,41.775608);

foreach ($tzList as $tz => $offset){
    $tzData = file_get_contents(sprintf('%s/data/%s.json', __DIR__, $tz));
    $timezone = geoPHP::load($tzData, 'json');

    printf(
        'This point %s within the timezone %s%s',
        $point->within($timezone) ? 'IS' : 'IS NOT',
        $tz,
        PHP_EOL
    );
}
