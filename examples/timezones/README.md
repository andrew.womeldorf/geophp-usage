# Timezone Lookup

Check if a set of coordinates is located within a timezone.

## Create Timezone Files

I included American Timezone files in repo, but they may be outdated, or irrelevant timezones.

Alternatively, you can download the timezone file for the whole world from the [Releases](https://github.com/evansiroky/timezone-boundary-builder/releases), but it's pretty large.

Use the [Timezone Boundary Builder](https://github.com/evansiroky/timezone-boundary-builder) tool to generate the GeoJSON file for timezones.

    - `git clone ...`
    - `npm i`
    - `node --max-old-space-size=8192 index.js --filtered-zones "America/New_York,America/Chicago,America/Denver,America/Los_Angeles,America/Anchorage,Pacific/Honolulu"`
